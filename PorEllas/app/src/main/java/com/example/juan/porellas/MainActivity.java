package com.example.juan.porellas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private JsonDecoder decoder;
    private GetRequest requester= new GetRequest();
    private EditText time;
    private EditText age;
    private EditText name;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    Context context;
    Toast toast;
    Button buttonLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        name = (EditText) findViewById(R.id.editName);
        age = (EditText) findViewById(R.id.editAge);
        time= (EditText) findViewById(R.id.editTime);
        radioSexButton=(RadioButton) findViewById(R.id.radioButton2);

        SharedPreferences prefs = getSharedPreferences("Data", MODE_PRIVATE);
        String user= prefs.getString("name","");
        String ages= prefs.getString("age","");
        String times=prefs.getString("time","15");
        String sex= prefs.getString("sex","m");

        if (sex.equals("h")){
            radioSexButton.setChecked(true);
        }


        name.setText(user);
        age.setText(ages);
        time.setText(times);


        buttonLogin = (Button) findViewById(R.id.button);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Button button = (Button) v;
                    button.setVisibility(View.INVISIBLE);
                    StarProcces();
                    button.setVisibility(View.VISIBLE);
            }
        });


    }

    void StarProcces(){
        //save preferences
        //buttonLogin.setVisibility(View.GONE);
        SharedPreferences prefs = getSharedPreferences("Data", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("name", name.getText().toString());
        editor.putString("age", age.getText().toString());
        editor.putString("time", time.getText().toString());
        if(radioSexButton.isChecked()){
            editor.putString("sex", "h");
        }else{
            editor.putString("sex", "m");
        }
        editor.commit();
        //get id
        decoder = new JsonDecoder(requester.request("http://acidtouch.com.mx/porellas/id.php"));
        //show id
        String id = decoder.GetVal("id");

        if (id==null){
            toast = Toast.makeText(context, "Error de conexion", Toast.LENGTH_LONG);
            toast.show();
        }else{
            Intent i = new Intent(this, ShareActivity.class);
            //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("id",id );
            startActivity(i);
        }



    }
}
