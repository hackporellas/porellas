package com.example.juan.porellas;

import android.content.Intent;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Crist on 08/10/2016.
 */

public class GetRequest {
    boolean error=false;
    StringBuilder result = new StringBuilder();
    public String request(final String GetRequest){
        final Thread RequestThread = new Thread(){
            public void run(){
                try{
                    URL url = new URL(GetRequest);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while((line = reader.readLine()) != null) {
                            result.append(line);
                        }
                        //Log.d("Login23",result.toString());
                        }catch (Exception e) {
                        error=true;

                    }finally {
                        urlConnection.disconnect();
                    }

                }catch (Exception e){
                    Log.d("Error23",e.toString());
                    error=true;

                }finally {

                }

            }
        };
        RequestThread.start();
        try{
            RequestThread.join();
        }catch (Exception e){
            error=true;
        }

        if (error==false ){
            Log.d("Login23",result.toString());
            return result.toString();
        }else{
            return null;
        }
    }




}
