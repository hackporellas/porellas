package com.example.juan.porellas;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.os.CountDownTimer;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class TimerActivity extends AppCompatActivity {
    int time=0;
    TextView timer,warning;
    String id="";
    Toast toast;
    String estado = "good"; //good panic finish
    String stime,lat,lng,name,sex,age,tiempo,limite;
    CountDownTimer cT,cT2;
    Context context;
    private Window wind;
    Button resetb,panicb,finishb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        Bundle b = getIntent().getExtras();
        id= b.getString("id");
        SharedPreferences prefs = getSharedPreferences("Data", MODE_PRIVATE);
        timer= (TextView) findViewById(R.id.textTimer);
        warning= (TextView) findViewById(R.id.warningtext);

        stime= prefs.getString("time","15");
        limite=stime;
        context = getApplicationContext();

        name= prefs.getString("name","");
        age= prefs.getString("age","");
        sex= prefs.getString("sex","");

        lat="0";
        lng="0";

        time = Integer.parseInt(stime) * 1000*60;
        setTimer();

        resetb = (Button) findViewById(R.id.buttonReset);
        resetb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stoptimers();
                estado="good";
                time = Integer.parseInt(stime) * 1000*60;

                setTimer();
            }
        });

        panicb = (Button) findViewById(R.id.buttonPanic);
        panicb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stoptimers();
                panic();
            }
        });

        finishb = (Button) findViewById(R.id.buttonFinish);
        finishb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stoptimers();
                finishSafe();
            }
        });

    }
    void setTimer(){
        cT =  new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                String v = String.format("%02d", millisUntilFinished/60000);
                int va = (int)( (millisUntilFinished%60000)/1000);
                timer.setText(v+":"+String.format("%02d",va));

                //Log.d("Actializacion",String.format("%02d",va));
                if (va % 20 == 0){
                    getLocation();
                    enviarActualizacion();
                    Log.d("Actializacion","se supone que se envio");


                }
            }
            public void onFinish() {
                timer.setText("Necesitas ayuda?");
                askAgain();
            }
        };
        cT.start();
    }

    void startWarning(){
        warning.setVisibility(View.GONE);
        cT =  new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                String v = String.format("%02d", millisUntilFinished/60000);
                int va = (int)( (millisUntilFinished%60000)/1000);
                warning.setText(v+":"+String.format("%02d",va));
            }
            public void onFinish() {
                timer.setText("Peligro!");
                panic();
            }
        };
        cT.start();
    }

    void stopWarning(){
        warning.setVisibility(View.GONE);
        cT2.cancel();
    }
    void panic(){
        estado="panic";
        time=10000000;
        setTimer();
    }

    void finishSafe(){
        stoptimers();
        estado="finish";
        enviarActualizacion();
        this.finish();
    }

    void stoptimers(){
        if (cT!=null){
            cT.cancel();
        }
        if (cT2!=null){
            cT2.cancel();
        }
    }

    void getLocation(){

    }

    void enviarActualizacion(){


    }

    void askAgain(){
        startWarning();
        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        boolean locked = km.inKeyguardRestrictedInputMode();
        if(locked){
            wind = this.getWindow();
            wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }else{
            Intent openMainActivity= new Intent(TimerActivity.this, TimerActivity.class);
            openMainActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(openMainActivity);
        }



        sound();
    }

    void sound(){
        Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(500);
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        stoptimers();
    }

}
