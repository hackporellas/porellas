package com.example.juan.porellas;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Time;

public class ShareActivity extends AppCompatActivity {
    String link;
    boolean shared=false;
    Context context;
    Toast toast;
    String id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        context = getApplicationContext();
        Bundle b = getIntent().getExtras();
        id= b.getString("id");
        link= "http://acidtouch.com.mx/porellas/index.php?id="+id;
        TextView text = (TextView) findViewById(R.id.textlink);
        text.setText(link);


        final ImageButton sharebutton = (ImageButton) findViewById(R.id.buttonShare);
        sharebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                shareProccess();
            }
        });

        final Button bstart = (Button) findViewById(R.id.button2);
        bstart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                starttimer();
            }
        });

    }

    void shareProccess(){
        shared=true;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = link;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    void starttimer(){
        if (shared==false){
            toast = Toast.makeText(context, "Compartelo al menos una vez", Toast.LENGTH_LONG);
            toast.show();
        }else{
            Intent i = new Intent(this, TimerActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("id",id );
            startActivity(i);
        }

    }
}
