package com.example.juan.porellas;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Crist on 08/10/2016.
 */

public class JsonDecoder {
    public boolean first;
    public String data;
    public JSONObject o;
    public JSONArray a;

    public JsonDecoder(String j){
        data=j;
        try{
            o = new JSONObject(j);
        }catch(Exception e){
            Log.d("JSON", "Error al convertir json");
        }
    }

    public String GetVal(String v){

        try{

            return o.getString(v);

        }catch(Exception e){
            Log.d("JSON", "Error al conseguir valor");
            return null;
        }
    }



}
